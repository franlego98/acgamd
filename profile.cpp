/*
 * acgamd : profile.cpp
 * 
 * Francisco Sanchez 2020
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
 
#include "profile.hpp"

Profile::Profile(){
	memset(data,0,sizeof(data));
}

Profile::Profile(uint8_t r,uint8_t g,uint8_t b){
	for(struct pixel_s *t = &data[0];
			t < &data[sizeof(data)/sizeof(struct pixel_s)];
			t++){
		t->r = r;
		t->g = g;
		t->b = b;
	}
}

int Profile::set_key(unsigned int index,uint8_t r,uint8_t g,uint8_t b){
	if(index >= MATRIX_SIZE) return -1;
	
	struct pixel_s *t = &data[index];
	
	t->r = r;
	t->g = g;
	t->b = b;
}

void Profile::fill(uint8_t r,uint8_t g,uint8_t b){
	for(struct pixel_s *t = &data[0];
			t < &data[sizeof(data)/sizeof(struct pixel_s)];
			t++){
		t->r = r;
		t->g = g;
		t->b = b;
	}
}

struct pixel_s Profile::get_key(unsigned int index){
	if(index >= MATRIX_SIZE) throw "Maximum key index!";
	
	return data[index];
}
