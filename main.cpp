/*
 * acgamd : main.hpp
 * 
 * Francisco Sanchez 2020
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
 
#include <iostream>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <signal.h>
#include <filesystem>
#include "keyboard.hpp"
#include "controlserver.hpp"
#include "keys_dict.hpp"
#include "update.hpp"

#define PROFILE_ONLINE 4 

using namespace std;

void exit_signal(int signal){
	control_end();
	exit(0);
}

void kbd_loop(Keyboard *kbd,boost::asio::deadline_timer *t){
	t->expires_from_now(boost::posix_time::milliseconds(1000));
	t->async_wait(boost::bind(kbd_loop, kbd, t));
	
	kbd->ping();
}

int	main(int argc,char* argv[]){
	
	//Keyboard Initialization
	Keyboard *kbd;
	
	try{
		kbd = new Keyboard(KEYBOARD_LANG_ES);
	}catch(const char* msg){
		cout << msg << endl;
		exit(-1);
	}
	
	kbd->ping();
	kbd->set_profile(PROFILE_ONLINE);
	kbd->current.fill(0,0,0);
	kbd->update_profile();
	
	cout << "Keyboard init!" << endl;  	
	
	//Keyboard ping thread (NECESSARY)
	boost::asio::io_context io;
	boost::asio::deadline_timer ping_timer(io, boost::posix_time::seconds(1));
	ping_timer.async_wait(boost::bind(kbd_loop, kbd, &ping_timer));
	
	//IPC control thread
	std::thread control_thread(control_loop,kbd);
	control_thread.detach();
	cout << "Listening socket on" << endl;
	
	
	//Update thread
	boost::asio::deadline_timer update_timer(io, boost::posix_time::milliseconds(250));
	update_timer.async_wait(boost::bind(update_loop, kbd, &update_timer));
	cout << "Updating colors from config" << endl;
	
	signal(SIGINT,exit_signal);
	
	io.run();
}
