/*
 * acgamd : keyboard.hpp
 * 
 * Francisco Sanchez 2020
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#ifndef _KEYBOARD_HPP_
#define _KEYBOARD_HPP_

#include <iostream>
#include <libusb-1.0/libusb.h>
#include <boost/thread/mutex.hpp>
#include "usbSettings.hpp"
#include "packet.hpp"
#include "stream.hpp"
#include "profile.hpp"
#include "keys_dict.hpp"

class Keyboard {
	libusb_device_handle *h;
	boost::mutex mtx_kbd;
	
	public:
		Keyboard(enum keyboard_lang lang);
		~Keyboard();
		int set_profile(int p);
		int ping();
		int update_profile();
		int read_profile();
		
	Profile current;
	map<string,unsigned int> keys_map;
};

#endif
