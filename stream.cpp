/*
 * acgamd : stream.cpp
 * 
 * Francisco Sanchez 2020
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include "stream.hpp"
 
Stream::Stream(libusb_device_handle *h,uint8_t h1,uint8_t h2){
	header1 = h1;
	header2 = h2;
	
	sent = 0;
	buff_filled = 0;
		
	memset(buff,0x00,56);
	
	handler = h;
}

int Stream::send_data(uint8_t *to_send,size_t s){
	int index = 0;
	int res;
	
	while(index < s){
		buff[buff_filled++] = to_send[index++];
		
		if(buff_filled == 56){
			
			uint8_t header[62];
			header[0] = header1;
			header[1] = header2;
			
			header[2] = sent&0xff;
			header[3] = (sent&0xff00)>>8;
			header[4] = (sent&0xff0000)>>16;
			
			header[5] = 56;
			
			sent+=56;
			buff_filled = 0;
			
			memcpy(&header[6],buff,56);
			
			Packet pkt(handler,header,62);
			if((res = pkt.send()) < 0);
				return res;
		}
	}
	
	return res;
}

Stream::~Stream() {
	if(buff_filled > 0){
		uint8_t header[62];
		memset(header,0x00,62);
		header[0] = header1;
		header[1] = header2;
		
		header[2] = sent&0xff;
		header[3] = (sent&0xff00)>>8;
		header[4] = (sent&0xff0000)>>16;
	
		header[5] = buff_filled;
	
		memcpy(&header[6],buff,buff_filled);
	
		Packet s(handler,header,buff_filled + 6);
		s.send();
	}
}
