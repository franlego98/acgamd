/*
 * acgamd : update.cpp
 * 
 * Francisco Sanchez 2020
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include "update.hpp"
#include <mpd/client.h>
#include <mpd/connection.h>
#include <mpd/status.h>

void update_loop(Keyboard *kbd,boost::asio::deadline_timer *t){
	t->expires_from_now(boost::posix_time::milliseconds(50));
	t->async_wait(boost::bind(update_loop, kbd, t));
	
	
	
	float val = 0;
	
	static struct mpd_connection *conn = NULL;
	
	if(conn == NULL){
		conn = mpd_connection_new(NULL, 0 ,30000);
	}
	
	if (mpd_connection_get_error(conn) == MPD_ERROR_SUCCESS) {
		struct mpd_status *status;
		status = mpd_run_status(conn);
		val = (float) mpd_status_get_volume(status)/100;
	}
	
	
	uint8_t color_start[3] = {0,0,0};
	uint8_t color_end[3] = {255,0,0};
	
	float bar_val = val;
	int bar_len = 22;
	for(int i = 0; i < bar_len;i++){
		uint8_t bar_part = 0;
		float segment = ((float) 1/bar_len);
		float segment_part = (bar_val - segment*i)/segment;
		
		if( bar_val >= segment*(i+1) ){
			segment_part = 1;
		}else if ( bar_val < segment*i ) {
			segment_part = 0;
		};
		
		kbd->current.set_key(22+i,
			color_end[0]*segment_part + ( 1 - segment_part)*color_start[0],
			color_end[1]*segment_part + ( 1 - segment_part)*color_start[1],
			color_end[2]*segment_part + ( 1 - segment_part)*color_start[2]);
		
	}
	kbd->update_profile();

}

