/*	
 * acgamd : packet.hpp
 * 
 * Francisco Sanchez 2020
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#ifndef _PACKET_HPP_
#define _PACKET_HPP_ 

#include <iostream>
#include <libusb-1.0/libusb.h>
#include <cstring>
#include "usbSettings.hpp"

extern "C" uint16_t crc_ccitt_ffff( const unsigned char *input_str, size_t num_bytes );

class Packet {
	private:
		libusb_device_handle *_h;
	
		struct layout_s {
			uint8_t first[6];
			uint16_t magic;
			uint8_t second[56];
		} layout;
	
	public:
		Packet(libusb_device_handle *h,uint8_t *to_send,size_t s);
		int send();
		uint8_t* response();
};

#endif
