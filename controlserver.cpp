/*
 * acgamd : controlserver.cpp
 * 
 * Francisco Sanchez 2020
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include "controlserver.hpp"

void control_client(stream_protocol::socket sock,Keyboard *kbd){
	try{
		boost::asio::streambuf obuf;
		boost::asio::streambuf ibuf;
		std::ostream osock(&obuf);

		osock << "ACGAMD V1.0" << endl;	
		boost::asio::write(sock, obuf);


	    boost::system::error_code ec;
		while(1){
			boost::asio::read_until(sock, ibuf,'\n',ec);
			
			if(ec == boost::asio::error::eof){
				sock.close();
				break;
			}
			
			string line = boost::asio::buffer_cast<const char*>(ibuf.data());
			ibuf.consume(line.length());
			
			if(line.compare(0,7,"keyname") == 0){
				uint r,g,b;
				char key_temp[100];
				if(sscanf(line.c_str(),"key %s %u,%u,%u",key_temp,&r,&g,&b) == 4){
					
					if(kbd->keys_map.count(key_temp) > 0){
						kbd->current.set_key(kbd->keys_map[key_temp],r,g,b);
						kbd->update_profile();
						osock << "OK" << endl;
					}else{
						osock << "BAD KEY NAME" << endl;
					}
				}else{
					osock << "BAD VALUES" << endl;
				}
			} else if(line.compare(0,7,"keycode") == 0){
				uint r,g,b,index;
				if(sscanf(line.c_str(),"key %u %u,%u,%u",&index,&r,&g,&b) == 4){
					
					if(kbd->current.set_key(index,r,g,b) == 0){
						kbd->update_profile();
						osock << "OK" << endl;
					}else{
						osock << "BAD KEY INDEX" << endl;
					}
				}else{
					osock << "BAD VALUES" << endl;
				}
			}else if(line.compare(0,4,"fill") == 0){
				uint r,g,b;
				if(sscanf(line.c_str(),"fill %u,%u,%u",&r,&g,&b) == 3){
					kbd->current.fill(r,g,b);
					kbd->update_profile();
					osock << "OK" << endl;
				}
			}else if(line.compare(0,5,"close") == 0){
				osock << "OK" << endl;
				break;
				sock.close();
			}else if(line.compare(0,11,"readprofile") == 0){
				kbd->read_profile();
			}else{
				osock << "UNKNOWN" << endl;
			}
			
			boost::asio::write(sock, obuf);
		}
	}catch (std::exception& e){
		std::cerr << "Exception in thread: " << e.what() << "\n";
	}
	
	cout << "Finished client thread" << endl;
}


void control_loop(Keyboard *kbd){
	boost::asio::io_context control_io;
	
	const string sock_path = "./test.sock";
	
	stream_protocol::endpoint ep(sock_path);
	stream_protocol::acceptor a(control_io,ep);
	filesystem::permissions(sock_path,
		filesystem::perms::others_read |
		filesystem::perms::others_write |
		filesystem::perms::group_read |
		filesystem::perms::group_write |
		filesystem::perms::owner_read |
		filesystem::perms::owner_write);
	
	while(1){
		std::thread(control_client, a.accept(),kbd).detach();
		cout << "Client thread detach" << endl;
	}
	
	cout << "exiting control_loop" << endl;
}

void control_end(){
	filesystem::remove("./test.sock");
}
