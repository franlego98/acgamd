/*
 * acgamd : stream.hpp
 * 
 * Francisco Sanchez 2020
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#ifndef _STREAM_HPP_
#define _STREAM_HPP_

#include <iostream>
#include <cstring>
#include "packet.hpp"

class Stream {
	libusb_device_handle *handler;
	
	uint8_t header1;
	uint8_t header2;
	
	int sent;
	int buff_filled;
	uint8_t buff[56];
	
	public:
		Stream(libusb_device_handle *h,uint8_t h1,uint8_t h2);
		int send_data(uint8_t *to_send,size_t s);
		~Stream();
};

#endif
