/*
 * acgamd : profile.hpp
 * 
 * Francisco Sanchez 2020
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#ifndef _PROFILE_HPP_
#define _PROFILE_HPP_
 
#include <iostream> 
#include <cstring>

#define MATRIX_SIZE 132

struct pixel_s {
	uint8_t r,g,b;
};

class Profile {
	private:
		struct pixel_s data[MATRIX_SIZE];
	
	public:
		Profile();
		Profile(uint8_t r,uint8_t g,uint8_t b);
		int set_key(unsigned int index, uint8_t r,uint8_t g,uint8_t b);
		void fill(uint8_t r,uint8_t g,uint8_t b);
		struct pixel_s get_key(unsigned int index);
};

#endif
