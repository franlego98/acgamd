/*
 * acgamd : keyboard.cpp
 * 
 * Francisco Sanchez 2020
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include "packet.hpp"

Packet::Packet(libusb_device_handle *h,uint8_t *to_send,size_t s){
	
	memset(&layout,0x00,64);
	
	if(s < 6){
		memcpy(&(layout.first),to_send,s);
	}else{
		memcpy(&(layout.first),to_send,6);
		if(s > 62) s = 62;
		memcpy(&(layout.second),&to_send[6],s-6);
	}
	
	uint16_t temp = crc_ccitt_ffff((unsigned char*) &layout.first,64);
	layout.magic = temp; 
	
	_h = h;
}

int Packet::send(){
	int exp,n;
	
	n = libusb_interrupt_transfer(
			_h,
			KEYBOARD_ENDPOINT,
			(unsigned char*) &layout,
			64,
			&exp,
			0);
			
	if(n < 0){
		std::cout << "Failed to transmit." << std::endl;
		return n;
	}
			
	n = libusb_interrupt_transfer(
			_h,
			HOST_ENDPOINT,
			(unsigned char*) &layout,
			64,
			&exp,
			0);
	
	if(n < 0)
		std::cout << "Failed to receive." << std::endl;
	
	return n;
}

uint8_t* Packet::response(){
	uint8_t* res = (uint8_t*) malloc(62);
	memcpy(res,layout.first,6);
	memcpy(&res[6],layout.second,56);
	return res;
}
