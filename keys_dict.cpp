/*
 * acgamd : keys_dict.hpp
 * 
 * Francisco Sanchez 2020
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include "keys_dict.hpp"

void init_dict(enum keyboard_lang lang,map<string,unsigned int> &k){
	if(lang == KEYBOARD_LANG_ES){
		k["esc"] 		= 0;
		
		k["f1"]			= 2;
		k["f2"]			= 3;
		k["f3"]			= 4;
		k["f4"]			= 5;
		k["f5"]			= 7;
		k["f6"]			= 8;
		k["f7"]			= 9;
		k["f8"]			= 10;
		k["f9"]			= 11;
		k["f10"]		= 12;
		k["f11"]		= 13;
		k["f12"]		= 14;
		k["imprpant"]		= 15;
		k["bloqdesp"]		= 16;
		k["pausainter"]	= 17;

		k["º"]			= 22;
		k["1"]			= 23;
		k["2"]			= 24;
		k["3"]			= 25;
		k["4"]			= 26;
		k["5"]			= 27;
		k["6"]			= 28;
		k["7"]			= 29;
		k["8"]			= 30;
		k["9"]			= 31;
		k["0"]			= 32;
		k["'"]			= 33;
		k["¡"]			= 34;
		k["backspace"]		= 36;

		k["ins"]		= 37;
		k["inicio"]		= 38;
		k["repag"]		= 39;

		k["bloqnum"]		= 40;
		k["/"]			= 41;
		k["*"]			= 42;
		k["-keypad"]		= 43;

		k["tab"]		= 44;
		k["q"]			= 45;
		k["w"]			= 46;
		k["e"]			= 47;
		k["r"]			= 48;
		k["t"]			= 49;
		k["y"]			= 50;
		k["u"]			= 51;
		k["i"]			= 52;
		k["o"]			= 53;
		k["p"]			= 54;
		k["`"]			= 55;
		k["+"]			= 56;
		k["}"]			= 58;
		
		k["supr"]		= 59;
		k["fin"]		= 60;
		k["avpag"]		= 61;
		
		k["7keypad"]		= 62;
		k["8keypad"]		= 63;
		k["9keypad"]		= 64;
		k["+keypad"]		= 65;

		k["bloqmayus"]		= 66;
		k["a"]			= 67;
		k["s"]			= 68;
		k["d"]			= 69;
		k["f"]			= 70;
		k["g"]			= 71;
		k["h"]			= 72;
		k["j"]			= 73;
		k["k"]			= 74;
		k["l"]			= 75;
		k["ñ"]			= 76;
		k["{"]			= 77;
		k["return"]		= 79;

		k["4keypad"]		= 84;
		k["5keypad"]		= 85;
		k["6keypad"]		= 86;

		k["mayusleft"]		= 88;
		k["<"]			= 89;
		k["z"]			= 90;
		k["x"]			= 91;
		k["c"]			= 92;
		k["v"]			= 93;
		k["b"]			= 94;
		k["n"]			= 95;
		k["m"]			= 96;
		k[","]			= 97;
		k["."]			= 98;
		k["-"]			= 99;
		k["mayus"]		= 102;

		k["up"]			= 104;

		k["1keypad"]		= 106;
		k["2keypad"]		= 107;
		k["3keypad"]		= 108;
		k["returnkeypad"]	= 109;

		k["ctrl"]		= 110;
		k["win"]		= 111;
		k["alt"]		= 112;
		k["space"]		= 116;
		k["altgr"]		= 120;
		k["fn"]			= 121;
		k["option"]		= 122;
		k["ctrlright"]		= 124;

		k["left"]		= 125;
		k["down"]		= 126;
		k["right"]		= 127;
		
		k["0keypad"]		= 128;
		k[".keypad"]		= 130;
		
	}
}
