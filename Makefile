LIBS += -lconfig++
LIBS += -lboost_program_options
LIBS += -lboost_system
LIBS += -lboost_random
LIBS += -lpthread
LIBS += -lusb-1.0
LIBS += -lmpdclient


main: packet.o stream.o packet.o keyboard.o libcrc profile.o controlserver.o keys_dict.o update.o
	g++ -o main $(LIBS) main.cpp stream.o packet.o keyboard.o libcrcccitt.o profile.o controlserver.o keys_dict.o update.o


packet.o: packet.cpp packet.hpp
	g++ -c -o packet.o packet.cpp

stream.o: stream.cpp stream.hpp packet.hpp
	g++ -c -o stream.o stream.cpp
	
profile.o: profile.cpp profile.hpp
	g++ -c -o profile.o profile.cpp
	
controlserver.o: controlserver.cpp controlserver.hpp
	g++ -c -std=c++17 -o controlserver.o controlserver.cpp
	
keyboard.o: keyboard.cpp keyboard.hpp packet.hpp stream.hpp profile.hpp
	g++ -c -o keyboard.o keyboard.cpp
	
keys_dict.o: keys_dict.cpp keys_dict.hpp
	g++ -c -o keys_dict.o keys_dict.cpp
	
update.o: update.cpp
	g++ -c -o update.o update.cpp
	
.PHONY : libcrc
libcrc:
	gcc -c -o libcrcccitt.o libcrc/crcccitt.c -Ilibcrc

