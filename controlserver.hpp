/*
 * acgamd : controlserver.hpp
 * 
 * Francisco Sanchez 2020
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#ifndef _CONTROLSERVER_HPP_
#define _CONTROLSERVER_HPP_

#include <iostream>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <filesystem>
#include "keyboard.hpp"
#include "profile.hpp"

using namespace std;
using boost::asio::local::stream_protocol;

void control_client(stream_protocol::socket sock,Keyboard *kbd);
void control_loop(Keyboard *kbd);
void control_end();

#endif
