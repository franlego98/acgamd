/*
 * acgamd : keyboard.cpp
 * 
 * Francisco Sanchez 2020
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include "keyboard.hpp" 

Keyboard::Keyboard(enum keyboard_lang lang) {
	int n = libusb_init(NULL);
	if(n < 0) std::cout << "Failed to init libusb." << std::endl;
	
	h = libusb_open_device_with_vid_pid(NULL,KEYBOARD_VID,KEYBOARD_PID);
	
	if(h == NULL){
        throw "Device not found. Sure it's connect and you can access it.";
	}else{
		std::cout << "Device found." << std::endl;
	}
	
	libusb_detach_kernel_driver(h,1);
	n = libusb_claim_interface(h,1);
	
	if(n < 0){
        throw "Failed to connect";
	}else{
		std::cout << "Device connected." << std::endl;
	}
	
	init_dict(lang,keys_map);
}

Keyboard::~Keyboard() {
	libusb_close(h);
	libusb_exit(NULL);
	std::cout << "Closing keyboard." << std::endl;
}

int Keyboard::set_profile(int p){
	mtx_kbd.lock();
	
	uint8_t temp[] = {0x0b,0x01};
	temp[1]+=p;
	
	Packet pkt(h,temp,2);
	
	mtx_kbd.unlock();
	return pkt.send();
}

int Keyboard::ping(){
	mtx_kbd.lock();
	
	uint8_t temp[] = {0x0c};
	
	Packet pkt(h,temp,1);
	
	mtx_kbd.unlock();
	return pkt.send();
}

int Keyboard::read_profile(){
	mtx_kbd.lock();
	
	uint8_t temp[] = {0x17,0x01};
	
	Packet pkt(h,temp,2);
	
	mtx_kbd.unlock();
	pkt.send();
	
	cout << "respuesta : " << to_string(pkt.response()[0]) << endl;
	
	return 1;
}

int Keyboard::update_profile(){
	mtx_kbd.lock();
	
	{
		Stream s(h,0x1a,0x01);
		for(int i = 0;i < MATRIX_SIZE;i++){
			struct pixel_s t = current.get_key(i);
			uint8_t raw[4] = {t.r,t.g,t.b,0xff};
			
			s.send_data(raw,4);
		}
		
	}
	
	uint8_t data[2] = {0x1a,0x02};
	Packet fin(h,data,2);
	fin.send();
	
	mtx_kbd.unlock();
	return 0;
}

